require 'logger'
require 'sinatra/base'

class App < Sinatra::Base
  set :port => 4000

  def logger
    @logger ||= Logger.new("#{settings.root}/log/#{settings.environment}.log")
  end

  before do
    query = env["QUERY_STRING"]
    msg = format("%s %s for %s",
                 env["REQUEST_METHOD"],
                 env["PATH_INFO"] + (query.empty? ? "" : "?#{query}"),
                 (env['HTTP_X_FORWARDED_FOR'] || env["REMOTE_ADDR"] || "-"))
    logger.info(msg)
  end

  get '/' do
    'Hello World!'
  end

  get '/api/v4/projects/40/packages/nuget/index.json' do
    json_response('service_index.json')
  end

  get '/api/v4/projects/40/packages/nuget/query' do
    json_response('query.json')
  end

  put '/api/v4/projects/40/packages/nuget/' do
    puts params.inspect
    puts JSON.pretty_generate(request.env)
    201
  end

  get '/api/v4/projects/40/packages/nuget/registration/mycompany.bananas/index.json' do
    json_response('metadata.json')
  end

  get '/api/v4/projects/40/packages/nuget/download/mycompany.bananas/index.json' do
    json_response('versions_short.json')
  end

  get '/api/v4/projects/40/packages/nuget/download/mycompany.bananas/1.0.0/mycompany.bananas.1.0.0.nupkg' do
    file_response('dummy.nupkg')
  end

  private

  def json_response(filename)
    content_type :json
    status 200
    File.open("#{File.dirname(__FILE__)}/responses/#{filename}", 'rb').read
  end

  def file_response(filename)
    content_type 'application/octet-stream'
    status 200
    File.read("#{File.dirname(__FILE__)}/responses/#{filename}")
  end

  run! if app_file == $0
end
