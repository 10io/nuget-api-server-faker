# NuGet API Server faker

This is a NuGet API Server faker.

It will serve static responses over NuGet API calls.

https://docs.microsoft.com/en-us/nuget/api/overview

# Authentication

To get/use username + password authentication, use https://ngrok.com with an authentified https tunnel.

# How to use this

* Install dependencies
```
$ bundle
```

* Start server
```
$ ruby app.rb
```
